<?php

/**
 * Settings form.
 */
function commerce_fast_ajax_atc_settings_form($form, &$form_state) {
  $form['commerce_fast_ajax_atc_use_confirmation_dialog'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use confirmation dialog'),
    '#default_value' => variable_get('commerce_fast_ajax_atc_use_confirmation_dialog'),
  );

  $blocks = _commerce_fast_ajax_atc_get_blocks_list(variable_get('theme_default'));
  $options = array();
  foreach ($blocks as $block) {
    $key = $block->module . ':' . $block->delta;
    $options[$key] = $key;
  }
  $form['commerce_fast_ajax_atc_cart_block'] = array(
    '#type' => 'select',
    '#title' => t('Cart block'),
    '#options' => $options,
    '#default_value' => variable_get('commerce_fast_ajax_atc_cart_block'),
  );
  
  $form['commerce_fast_ajax_atc_cart_block_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Cart block html id'),
    '#default_value' => variable_get('commerce_fast_ajax_atc_cart_block_id'),
  );

  return system_settings_form($form);
}

/**
 * Return blocks list.
 */
function _commerce_fast_ajax_atc_get_blocks_list($theme) {
  return db_query("
    SELECT module, delta
    FROM {block}
    WHERE theme = :theme
    ORDER BY module, delta
  ", array(':theme' => $theme))->fetchAll();
}
