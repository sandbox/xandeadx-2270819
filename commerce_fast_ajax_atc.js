(function ($) {
  Drupal.behaviors.commerceFastAjaxAtc = {
    attach: function (context, settings) {
      $('.use-fast-ajax-submit:not(.ajax-processed)').addClass('ajax-processed').each(function () {
        var base = $(this).attr('id');
        Drupal.ajax[base] = new Drupal.ajax(base, this, {
          url: $(this.form).attr('action'),
          event: 'click',
          progress: {'type':'throbber'}
        });
      });
    }
  };
})(jQuery);
